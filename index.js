
const config = require("./config.json");
const Discord = require('discord.js');
const fs = require("fs");
const client = new Discord.Client();

client.login(config.token);
//client.options.sync = true; // refresh guild members every 30s
const ROLES = [];
const ROLE_NAMES = config.roleNames;

if (ROLE_NAMES.length < 1) {
  console.error('no role name provided')
  process.exit(1);
}


client.on('message', message => {
  if (message.channel.type === 'dm') {  // dm = direct message
    console.log('direct message recieved');

    const blockEntry = config.blockedUsers.find(user =>
      user.id === message.author.id
    )

    if (blockEntry && blockEntry.tries >= config.maxTries) {
      console.log("warned a blocked user: " + blockEntry.id);
      // TODO: warn mods
      addTry(blockEntry);
    } else {
      const input = message.content.trim();

      if (config.validKeys.includes(input)) {
        addRoles(message.author, input);
      } else {
        if (config.usedKeys.includes(input)) {
          // TODO: warn mods
          if (config.revokeOnKeyShare) {
              let user = config.registeredUsers.find(user => user.key === input);

              removeRoles(blockEntry, user.id);

              console.warn(`duplicated key found: ${input} removed roles from ${user.id} (${user.username})`);
          }
        }
        addTry(blockEntry, message.author);
      }
    }
  }
});

process.on('exit', () => {
  console.log('logging out');
  client.disconnect();
});

client.on('disconnect', () => {
  console.log('successfully logged out');
});

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);

  ROLE_NAMES.forEach(name => {
      const role = client.guilds.get(config.guildID).roles.find("name", name);

      if (role === undefined) {
        console.error('role not found: ' + name);
        process.exit(1);
      } else {
        ROLES.push(role);
      }
  })
});

function saveConfig() {
  fs.writeFile("./config.json",
    JSON.stringify(config, null, 4),
    err => console.error
  );
}

function addTry(blockEntry, author) {
    if (blockEntry) {
    blockEntry.tries++;
  } else {
    config.blockedUsers.push({
      id: author.id,
      name: author.username,
      tries: 1
    });
  }

  saveConfig();
}

function removeRoles(blockEntry, userID) {
  const guildMember = client.guilds.get(config.guildID).members.get(userID);

  ROLES.forEach(role => {
    guildMember.removeRole(role, 'another user tried to user your key, moderators are informed.');
    console.log(`removed ${role.name} role from user ${guildMember.user.id} (${guildMember.user.username})`);
  });

  config.registeredUsers.filter(user =>  user.id !== userID);
  saveConfig();

  addTry(blockEntry, guildMember.user);
}


function addRoles(author, input) {

  const guildMember = client.guilds.get(config.guildID).members.get(author.id);

  ROLES.forEach(role => {
    guildMember.addRole(role);
    console.log(`added ${role.name} role to user ${author.id} (${author.username})`);
  });

  config.registeredUsers.push({
    id: guildMember.user.id,
    username: guildMember.user.username,
    key: input
  });
  config.validKeys.splice(config.validKeys.indexOf(input), 1);
  config.usedKeys.push(input);
  saveConfig();
  console.log("marked key as used");
}
